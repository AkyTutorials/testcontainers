package example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testcontainers.containers.BrowserWebDriverContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.File;

@Testcontainers(disabledWithoutDocker = true, parallel = true)
public class WebDriver {

    BrowserWebDriverContainer cont;

    @BeforeEach
    public void setup(){
        cont = new BrowserWebDriverContainer().withCapabilities(new ChromeOptions())
                .withRecordingMode(BrowserWebDriverContainer.VncRecordingMode.RECORD_ALL, new File("build"));
        cont.start();
    }

    @Test
    public void test1() throws Exception{
        RemoteWebDriver driver = cont.getWebDriver();
        driver.get("http://www.google.com");
        WebElement textBox = driver.findElement(By.xpath("//textarea[@title='Search']"));
        WebElement defaultButton = driver.findElement(By.name("btnK"));
        textBox.sendKeys("Apple");
        System.out.println(defaultButton.getAttribute("value"));
    }

}
