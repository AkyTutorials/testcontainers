package example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Testcontainers(disabledWithoutDocker = true, parallel = true)
public class SimpleWebServer {

    public GenericContainer simpleWebServer;

    @BeforeEach
    public void setUp() {
        simpleWebServer
                = new GenericContainer("alpine:3.2")
                .withExposedPorts(8080)
                .withCommand("/bin/sh", "-c", "while true; do echo "
                        + "\"HTTP/1.1 200 OK\n\nHello World!\" | nc -l -p 8080; done");
    }

    @Test
    public void testSimplePutAndGet() throws Exception {
        simpleWebServer.start();
        System.out.println(simpleWebServer.isHostAccessible() + " " + simpleWebServer.isRunning());
        String host = simpleWebServer.getHost();
        int port = (int) simpleWebServer.getFirstMappedPort();
        String urlStr = "http://" + host + ":" + port;
        System.out.println(urlStr);
        System.out.println("response : " + simpleGetRequest(urlStr));
    }

    public String simpleGetRequest(String urlStr) throws Exception{
        String inputLine;
        StringBuffer content = new StringBuffer();
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        conn.disconnect();
        return content.toString();
    }
}
