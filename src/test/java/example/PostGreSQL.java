package example;

import org.junit.jupiter.api.*;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

@Testcontainers(disabledWithoutDocker = true, parallel = true)
public class PostGreSQL {

    static PostgreSQLContainer cont = new PostgreSQLContainer("postgres:12.7");
    static Connection conn;

    @BeforeAll
    public static void setup() throws Exception{
        cont.start();
        String jdbcUrl = cont.getJdbcUrl();
        String username = cont.getUsername();
        String password = cont.getPassword();
        System.out.println(jdbcUrl + " " + username + " " + password);
        Class.forName("org.postgresql.Driver");
        conn = DriverManager.getConnection(jdbcUrl, username, password);
        setupData();
    }


    @Test
    public void test1() throws Exception{
        ResultSet resultSet = conn.createStatement().executeQuery("SELECT * from employee");
        resultSet.next();
        System.out.println(resultSet.getString(1) + " " + resultSet.getString(2) + " "
                + resultSet.getString(3));
    }

    @AfterAll
    public static void destruct() throws Exception{
        conn.close();
    }

    static void setupData() throws Exception{
        String emp_table = "Create Table if not exists employee (" +
                "id int not null," +
                "name varchar not null," +
                "department varchar not null" +
                ");";
        var statment = conn.prepareStatement(emp_table);
        int result = statment.executeUpdate();
        System.out.println("Create Table Result : " + result);
        String ins_emp = "insert into employee values (1, 'Akshay Goel', 'IT');";
        statment = conn.prepareStatement(ins_emp);
        result = statment.executeUpdate();
        System.out.println("Insert into Table Result : " + result);
    }
}
