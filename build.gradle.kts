plugins {
    id("java")
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.9.1"));
    testImplementation("org.junit.jupiter:junit-jupiter");
    testImplementation("org.junit.jupiter:junit-jupiter:5.8.1");
    testImplementation("org.testcontainers:testcontainers:1.18.3");
    testImplementation("org.testcontainers:junit-jupiter:1.18.3");
    testImplementation("org.testcontainers:postgresql:1.18.3");
    testImplementation("org.testcontainers:selenium:1.18.3");
    testImplementation("org.slf4j:slf4j-api:2.0.7");
    testImplementation("org.slf4j:slf4j-simple:2.0.7");
    testImplementation("org.postgresql:postgresql:42.2.5");
    testImplementation("org.seleniumhq.selenium:selenium-remote-driver:4.10.0");
    testImplementation("org.seleniumhq.selenium:selenium-firefox-driver:4.10.0");
    testImplementation("org.seleniumhq.selenium:selenium-chrome-driver:4.10.0");
    testImplementation("org.testcontainers:selenium");
    testImplementation("org.assertj:assertj-core:3.24.2");
}

tasks.test {
    useJUnitPlatform()
}